'use strict';

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function checkInput(answer){
	return isNaN(answer);
}

function promiseInput(curMax = "") {
	return new Promise((resolve, reject) => {
		rl.resume();
		let input = rl.question('Write a word', answer => {
			rl.pause();
			if (checkInput(answer)) {
				let maxLen = Math.max(curMax.length, answer.length);
				resolve(answer.length == maxLen ? answer : curMax);
			} else {
				reject(new Error("It's number"));
			}
			return answer;
		});
		return input;
	});
}

promiseInput()
.then((resolved)=>promiseInput(resolved))
.then((resolved)=>promiseInput(resolved))
.then((resolved)=>promiseInput(resolved))
.then((resolved)=>promiseInput(resolved))
.then(function(result){
	console.log(result);
	console.log(result.length);
})
.catch();