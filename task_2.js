"use strict";

function findFirstLetters(str) { //создает массив первых букв каждого слова
    return str.split(" ").map(function(word){
        return word[0];
    });
}

function makeElementsUnique(array){ // сортирует массив и оставляет только уникальные значения
    array.sort((a,b) => a - b);
    let result = [];
    for(let number of array){
        if(!result.includes(number)){
            result.push(number);
        }
    }
    return result;
}

function someFilter(array){ //удаляет ложные значения
    let result = array.filter( function(value){
        return value;
    });
    return result;
}