"use strict";

let first = +process.argv[2];
let second = +process.argv[3];

if (isNaN(first)) {
    console.log("Первый ввод - не число");
} else if (isNaN(second)) {
    console.log("Второй ввод - не число");
} else {
    if (first==second) {
        console.log("Числа равны");
    } else if (first > second) {
        console.log("Первое число больше");
    } else {
        console.log("Второе число больше");
    }
}