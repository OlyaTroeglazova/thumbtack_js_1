"use strict";

function helloWorld(speech){
    if(speech=='ru'){
        return 'Привет мир!';
    }
    return 'Hello World!'
}

function inArray(array = [], number = ''){
    for(let value of array){
        if (value == number){
            return true;
        }
    }
    return false;
}
