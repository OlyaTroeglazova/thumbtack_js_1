'use strict';

class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    display() {
        console.log(this.name);
        console.log(this.age);
    }
}

class Employee extends Person {
    constructor(name, age, company) {
        super(name, age);
        this.company = company;
    }

    display() {
        super.display();
        console.log(this.company);
    }

    work() {
        if(typeof this.company === 'undefined' || this.company == '' || this.company == null || this.company == false){
            console.log(this.name + " is not working now");
        } else {
            console.log(this.name + " is working now.");
        }
    }
}