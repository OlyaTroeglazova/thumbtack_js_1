'use strict';

class User {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
    getFullname() {
        return this.name + " " + this.surname;
    }
}

class Student extends User {
    constructor(name, surname, year) {
        super(name, surname);
        this.year = year;
    }

    getCourse() {
        let date = new Date;
        let course = date.getFullYear() - this.year + 1;
        if (course < 1) {
            console.log("Студент еще не поступил в вуз");
        } else if (course > 5) {
            console.log("Студент уже окончил вуз");
        } else{
            return course;
        }
    }
}