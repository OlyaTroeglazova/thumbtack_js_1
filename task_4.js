"use strict";

function exclude (array, ...args){
    for(let number of args){
        if(!array.includes(number)){
            array.push(number);
        }
    }
    return array;
}
